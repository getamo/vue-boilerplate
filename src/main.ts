import { Widget } from './widget'

export default function (amoWidget: any) {
  let widget: Widget
  const getWidget = async () => {
    if (!widget) {
      widget = new Widget(amoWidget)
    }

    return widget
  }

  amoWidget.callbacks = {
    settings() {
      if (amoWidget.params.status === 'not_configured') {
        amoWidget.background_install()
      }
    },
    init() {
      return true
    },
    bind_actions() {
      return true
    },
    async render() {
      ;(await getWidget()).onRender()
      return true
    },
    dpSettings() {
      return true
    },
    advancedSettings() {
      return true
    },
    destroy() {},
    contacts: {
      selected() {
        return true
      }
    },
    leads: {
      async selected() {
        const { selected } = amoWidget.list_selected()
        const leadIds: number[] = selected.map(({ id }: { id: number }) => id)
        console.debug(leadIds)
        return true
      }
    },
    todo: {
      selected() {
        return true
      }
    },
    onSave() {}
  }

  return amoWidget
}
