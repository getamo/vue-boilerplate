import type { App, AsyncComponentLoader } from 'vue'
import { create } from 'amo-client-axios'
import type { AxiosInstance } from 'axios'
import ShortUniqueId from 'short-unique-id'

export class Widget {
  constructor(private amoWidget: any) {
    this.api = create(this.clientId, {
      baseURL: import.meta.env.API_URL
    })
  }

  private modalApp!: App
  private uniqId = `modal-${new ShortUniqueId()}`
  private api: AxiosInstance

  onRender() {
    return true
  }

  get clientId() {
    return this.amoWidget.params.oauth_client_uuid
  }

  get isCard(): boolean {
    return APP.isCard() as boolean
  }

  get isLeads(): boolean {
    return APP.getBaseEntity() === 'leads'
  }

  private async createApp(component: AsyncComponentLoader, props?: any) {
    const { createApp, defineAsyncComponent } = await import('vue')
    const { createPinia } = await import('pinia')
    const app = createApp(defineAsyncComponent(component), props)
    app.use(createPinia())
    return app
  }
}
