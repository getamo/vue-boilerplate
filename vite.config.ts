import { fileURLToPath, URL } from 'node:url'

import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueDevTools from 'vite-plugin-vue-devtools'

export default defineConfig(({ mode }) => {
  // Load env file based on `mode` in the current working directory.
  // Set the third parameter to '' to load all env regardless of the `VITE_` prefix.
  const env = loadEnv(mode, process.cwd(), '')
  const publicPath = env.PUBLIC_PATH || `http://localhost:${env.VITE_PORT}`

  return {
    base: publicPath,
    plugins: [vue(), vueDevTools()],
    resolve: {
      alias: {
        '@': fileURLToPath(new URL('./src', import.meta.url))
      }
    },
    server: {
      port: Number(env.VITE_PORT)
      // https: {},
      // headers: {
      //   'Access-Control-Allow-Origin': '*',
      //   'Access-Control-Allow-Headers': '*',
      //   'Access-Control-Allow-Methods': '*'
      // }
    },
    build: {
      outDir: 'dist/js',
      minify: 'terser',
      rollupOptions: {
        output: {
          inlineDynamicImports: true,
          exports: 'named'
        }
      },
      terserOptions: {
        compress: {
          drop_console: true,
          drop_debugger: true
        }
      },
      lib: {
        formats: ['umd'],
        entry: fileURLToPath(new URL('./src/main.ts', import.meta.url)),
        name: 'widget',
        fileName: (format) => `widget.${format}.js`
      }
    }
  }
})
