import { Config, WidgetBuilder } from 'amo-widget-builder'
import { config as commonConfig } from './common'

const publicPath = process.env.PUBLIC_PATH || `http://localhost:${process.env.VITE_PORT}`

const config: Config = {
  ...commonConfig,
  entryPoint: `${publicPath}/dist/js/widget.umd.js`
}

const builder = new WidgetBuilder(config)

;(async () => {
  await builder.init()
})()
